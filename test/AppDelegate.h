//
//  AppDelegate.h
//  test
//
//  Created by hua on 13-9-5.
//  Copyright (c) 2013年 hua. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
