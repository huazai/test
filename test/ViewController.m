//
//  ViewController.m
//  test
//
//  Created by hua on 13-9-5.
//  Copyright (c) 2013年 hua. All rights reserved.
//

#import "ViewController.h"
#import "testViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(100, 100, 30, 30);
    [button setTitle:@"test" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (void)buttonTap
{
    testViewController *test = [[testViewController alloc] init];
    [self presentViewController:test animated:test completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
